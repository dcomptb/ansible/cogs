# Cogs Ansible Role

This role installs and configures a [Cogs](https://gitlab.com/dcomptb/cogs) system.

## Variables

| name | required | default | description |
| ---- | ---------| ------- | ----------- |
| tbxir | **yes** | | testbed xir model |
| bgp\_addr | **yes** | | BGP address of the cog host |
| bgp\_as | **yes** | | BGP AS number of the cog host |
| bgp\_id | **yes** | | BGP id (typically loopback addr) |
| bgp\_peer\_as | **yes** | | AS number of peer |
| tbifx | **yes** | | Interface on the testbed infra net |
| extifx | **yes** | | Interface facing the internet |
| ext\_addr | **yes** | | External address (used for experiment NAT) |
| ext\_gw | **yes** | | External gateway (used for experiment NAT) |
| ext\_subnet | **yes** | | External subnet (used for experiment NAT) |
| etcd\_ca | **yes** | | etcd certificate authority |
| etcd\_cert | **yes** | | etcd certificate |
| etcd\_key | **yes** | | etcd key |
| etcd\_host| no | localhost | etcd host address |
| etcd\_port| no | 2399 | etcd host port |
| cmdr\_host| no | localhost | commander host |
| cmdr\_port| no | 6000 | commander port |
| cmdr\_cert | **yes** | | commander certificate |
| driver\_host| no | localhost | driver host |
| driver\_port| no | 6000 | driver port |
| foundry\_cert | **yes** | | foundry certificate |
| foundry\_key | **yes** | | foundry key |
| beluga\_host| no | localhost | beluga host address |
| beluga\_port| no | 5402 | beluga host port |

## Role dependencies

This role depends on the following roles which must be installed independently.

- [Canopy](https://gitlab.com/mergetb/ansible/canopy)
- [Gobble](https://gitlab.com/mergetb/ansible/gobble)

## Example

```yaml
import_role:
  name: cogs
vars:
  tbxir: tb-model.json
  bgp_addr: 10.99.0.5/32
  bgp_as: 64705
  bgp_id: 10.99.0.5
  tbifx: eth1
  ext_addr: 1.2.3.4
```
